import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dev on 2/12/14.
 */
public class ThreadExample
{
    public static void main(String[] args)
    {
        MainFrame frame = new MainFrame();

        frame.setSize(500,200);
        frame.setLayout(new FlowLayout());
        frame.createUI();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
    }

}
