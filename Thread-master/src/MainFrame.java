import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.Thread;


/**
 * Created by dev on 2/12/14.
 */
public class MainFrame extends JFrame
{
    private MyThread myThread;
    private JTextField textField;
    private JButton btnCreate;
    private JButton btnStart;
    private JButton btnStop;
    public MainFrame()
    {
        //
    }

    public void createUI()
    {
        textField = new JTextField();
        textField.setPreferredSize(new Dimension(100,30));
        this.add(textField);

        btnCreate = new JButton("Create Thread");
        this.add(btnCreate);

        btnStart = new JButton("Start Thread");
        this.add(btnStart);

        btnStop = new JButton("Stop Thread");
        this.add(btnStop);

        btnCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myThread = new MyThread(textField);
                btnCreate.setEnabled(false);
                System.out.println("Thread created");
            }
        });

        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myThread.startThread();
            }
        });

        btnStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myThread.stopThread();
            }
        });
    }
}
