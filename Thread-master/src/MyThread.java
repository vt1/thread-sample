import javax.swing.*;

/**
 * Created by dev on 2/12/14.
 */
public class MyThread implements Runnable {
    private Thread thread;
    private JTextField textField;
    private boolean isStoped;

    public MyThread(JTextField textField)
    {
        thread = new Thread(this, "My Thread");
        this.textField = textField;
    }

    public void startThread()
    {
        if(!thread.isAlive())
            thread.start();
        isStoped = false;
    }


    public void stopThread()
    {
        isStoped = true;
    }

    @Override
    public void run()
    {
        try
        {
            int cnt = 0;
            while (true)
            {
                if(!isStoped)
                {
                    cnt++;
                    textField.setText(Integer.toString(cnt));
                    Thread.sleep(500);
                }
            }
        }
        catch (Exception e){

        }

    }
}
